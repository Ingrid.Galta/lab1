package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    Random rand = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.println(String.format("Let's play round %d", roundCounter));
            String humanChoice = userChoice();
            String computerChoice = randomChoice();
            String choiceString = String.format("Human chose %s, computer chose %s.", humanChoice, computerChoice);

            if (isWinner(humanChoice, computerChoice)){
                System.out.println(String.format("%s Human wins!", choiceString));
                humanScore += 1;
            }
            else if (isWinner(computerChoice, humanChoice)){
                System.out.println(String.format("%s Computer wins!", choiceString));
                computerScore += 1;
            }
            else{
                System.out.println(String.format("%s it's a tie", choiceString));
            }
            System.out.println(String.format("Score: human %d, computer %d", humanScore, computerScore));

            String continueAnswer = continuePlaying();
            if (continueAnswer.equals("n")){
                break;


            }

        }
        System.out.println("Bye bye :)");

    }

    public String randomChoice(){
        return rpsChoices.get(rand.nextInt(rpsChoices.size()));
    }

    public boolean isWinner(String choice1, String choice2){
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")){
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }

    }

    public String userChoice(){
        while (true){
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            if (validateInput(humanChoice, rpsChoices)){
                return humanChoice;

            }
            else {
                System.out.println(String.format("I don't understand %s. Try again.", humanChoice));
            }

        }
    }

    public String continuePlaying(){
        while (true){
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?");
            if (validateInput(continueAnswer,Arrays.asList("y", "n"))){
                return continueAnswer;
            }
            else{
                System.out.println(String.format("I don´t understand %s. Try again.", continueAnswer));
            }
    

        }
    }

    public Boolean validateInput(String input, List<String> validInput){
        input = input.toLowerCase();
        return validInput.contains(input);

    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
